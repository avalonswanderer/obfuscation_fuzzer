# Explanation

The file antifuzz.h was created using the following command line with Antifuzz:

```bash
$ ./antifuzz.py --anti-coverage 100 --signal  --crash-action exit
```

By adding a call to antifuzz_init in the main function of antifuzz_bitfill_test.c and the include to antifuzz.h, we have setup the antifuzzing process. We have, then, applied Tigress 3.1 using the command line:

```bash
$ tigress --Environment=x86_64:Linux:Gcc:4.6 --Seed=0 --Transform=Split --SplitKinds=deep,block,top --SplitCount=100 --Functions=antifuzz_init,main --out=antifuzz_bitfill_tigress.c antifuzz_bitfill_test.c
```
By searching the call to the init function in the newly generated binary we can locate the actual call instruction and replaced it by NOP instruction which gives us the new binary antifuzz_bitfill_test_reversed_noMoreAntifuzz. This allows us to bypass the antifuzzing protection.